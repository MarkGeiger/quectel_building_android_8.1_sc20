# Quectel_Building_Android_8.1_SC20

How to compile Android with the Quectel and Android SDK on **Ubuntu 20.04/20.10**

## Prepare env 

```
sudo apt-get install git-core gnupg flex bison build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig openjdk-8-jdk libncurses5 libssl-dev
```

Be sure that java8 is the sdk and runtime selected by default (JAVA_HOME), you can configure by:

```
sudo  update-alternatives --config javac
sudo  update-alternatives --config java
sudo  update-alternatives --config javadoc
```

Also install repo and git: 
```
sudo apt-get install repo git
```

The repo tool sometimes has the problem that it uses python3 instead of 2.7. Sadly it seems to not work like it should with python3. Please make sure that "repo --version" states that python 2.7 is used.

(maybe required to clone caf-stable) prepare gpg keys:

```
mkdir -p ~/.repoconfig/gnupg/
GNUPGHOME=~/.repoconfig/gnupg/ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 692B382C
```

**Carefull. Do not clone onto NTFS partitions, it will not work. SDK has invalid folder names for NTFS (e.g. "aux")**

_(Hint: you can also ask Quectel FAEs to provide a pre-merged SDK download link, which will be faster to clone)_

Clone the android sdk:
```
repo init -u https://source.codeaurora.org/quic/la/platform/manifest.git -b release -m LA.UM.6.7.r1-07200-8x09.0.xml --repo-url=git://codeaurora.org/tools/repo.git –repo-branch=caf-stable
```

Sync the repo. This will take long time. **Careful the complete clone takes up around 40GB of disk space**.
```
repo sync
```
**Uncompress Quectel SDK (provided by Quctel FAEs) in same directory where we just cloned the Android SDK**

Add locale or flex will generate errors.
```
export LC_ALL=C
```

fix dtc parser according to:
https://lkml.org/lkml/2020/4/1/1206

in short, open the file: ./sdk/kernel/msm-3.18/scripts/dtc/dtc-parser.tab.c_shipped
find the definition of "YLTYPE yylloc;" (should be at line 73) and add a "extern".

```
- YYLTYPE yylloc
+ extern YYLTYPE yylloc;
```

Now you are ready to build, careful lots of target files will generated. **A finished build with this SDK takes up around 100GB of disk space, be sure that there is enough free space to build**:
```
source build/envsetup.sh
lunch msm8909-userdebug
make –j
```
